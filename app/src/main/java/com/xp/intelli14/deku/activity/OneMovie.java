package com.xp.intelli14.deku.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.graphics.drawable.DrawableCompat;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.xp.intelli14.deku.R;
import com.xp.intelli14.deku.db.DBHandler;
import com.xp.intelli14.deku.model.Result;
import com.xp.intelli14.deku.property.StaticMemory;

public class OneMovie extends AppCompatActivity {


    private ImageView oneMovieImageView, oneMovieMoviePoster;
    private View oneMovieGradiant;
    private StaticMemory staticMemory = StaticMemory.getInstance();
    private TextView oneMovieTitle, oneMovieDesc;
    private AppCompatButton savedButton;
    private Gson gson = new Gson();
    private DBHandler dbHandler;


    @SuppressLint("ResourceAsColor")
    private void init(){
        oneMovieImageView = findViewById(R.id.oneMovieImageView);
        oneMovieGradiant = findViewById(R.id.oneMovieGradiant);
        oneMovieMoviePoster = findViewById(R.id.oneMovieMoviePoster);
        oneMovieTitle = findViewById(R.id.oneMovieTitle);
        oneMovieDesc = findViewById(R.id.oneMovieDesc);
        savedButton = findViewById(R.id.saveButton);
        dbHandler = new DBHandler(this);

        Result currentMovie = staticMemory.currentlyViewableMovie;
        if(currentMovie.backdrop_path != null &&  !currentMovie.backdrop_path.isEmpty())
            Picasso.get().load(staticMemory.INITIAL_IMAGE_URL.concat(currentMovie.backdrop_path)).into(oneMovieImageView);
        if(currentMovie.poster_path != null &&  !currentMovie.poster_path.isEmpty())
            Picasso.get().load(staticMemory.INITIAL_IMAGE_URL.concat(currentMovie.poster_path)).into(oneMovieMoviePoster);
        int overLimit = 150;
        oneMovieTitle.setText(currentMovie.title);
        if(currentMovie.overview.length() > overLimit){
            oneMovieDesc.setText(currentMovie.overview.substring(0,overLimit-3).concat("..."));
        }else{
            oneMovieDesc.setText(currentMovie.overview);
        }

        String p = dbHandler.getKeyViaIdSaved(currentMovie.id);
        if(!p.isEmpty()){
            DrawableCompat.setTintList(savedButton.getBackground(), ColorStateList.valueOf(R.color.c_1));
            savedButton.setText("Movie already bookmarked");
            savedButton.setClickable(false);
        }

        savedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(p.isEmpty()){
                    boolean a = dbHandler.addOneSaved(currentMovie.id, "SAVED_MOVIE", gson.toJson(currentMovie));
                    Toast.makeText(v.getContext(), "Movie saved offline", Toast.LENGTH_SHORT).show();
                    DrawableCompat.setTintList(savedButton.getBackground(), ColorStateList.valueOf(R.color.c_1));
                    savedButton.setText("Movie already bookmarked");
                    savedButton.setClickable(false);
                }
            }
        });

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_movie);
        init();

    }
}