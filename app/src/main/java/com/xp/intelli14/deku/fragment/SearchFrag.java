package com.xp.intelli14.deku.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.xp.intelli14.deku.R;
import com.xp.intelli14.deku.adapter.GenMovieListAdapter;
import com.xp.intelli14.deku.model.ResponseGen;
import com.xp.intelli14.deku.property.StaticMemory;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SearchFrag#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchFrag extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public SearchFrag() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SearchFrag.
     */
    // TODO: Rename and change types and number of parameters
    public static SearchFrag newInstance(String param1, String param2) {
        SearchFrag fragment = new SearchFrag();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private LinearLayout serachBarLinear;
    private EditText searchMoviewEditText;
    private AppCompatButton searchMoviewButton;
    private RecyclerView searchResultRecyclerView;
    private StaticMemory staticMemory = StaticMemory.getInstance();
    private Gson gson = new Gson();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view  = inflater.inflate(R.layout.fragment_search, container, false);

        serachBarLinear = view.findViewById(R.id.serachBarLinear);
        searchMoviewEditText = view.findViewById(R.id.searchMoviewEditText);
        searchMoviewButton = view.findViewById(R.id.searchMoviewButton);
        searchResultRecyclerView = view.findViewById(R.id.searchResultRecyclerView);

        searchMovie("1", 1, "batman", view);

        searchMoviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = searchMoviewEditText.getText().toString();
                if(str != null && !str.isEmpty()){
                    searchMovie("1", 1, str, view);
                }
            }
        });

        return view;
    }

    private void searchMovie(String pageNumber, int requestNumber, String searchString, View rootView){
        String uri = staticMemory.SEARCH_URL
                .replace(staticMemory.PAGE_NUMBER_PARAM, pageNumber)
                .replace(staticMemory.SEARCH_STRING, searchString);
        Request request = new Request.Builder()
                .url(uri)
                .build();

        Call call = staticMemory.client.newCall(request);

        call.enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            //Toast.makeText(getApplication(), response.body().string(), Toast.LENGTH_SHORT).show();
                            ResponseGen responseGen = gson.fromJson(response.body().string(), ResponseGen.class);
                            refreshList(responseGen, rootView);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }

    private void refreshList(ResponseGen responseGen, View view){
        GenMovieListAdapter adapter = new GenMovieListAdapter(responseGen.results, getActivity());
        searchResultRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        searchResultRecyclerView.setAdapter(adapter);
    }
}