package com.xp.intelli14.deku.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.xp.intelli14.deku.R;
import com.xp.intelli14.deku.activity.OneMovie;
import com.xp.intelli14.deku.activity.ShowMore;
import com.xp.intelli14.deku.adapter.GenMovieListAdapter;
import com.xp.intelli14.deku.adapter.NowPlayingAdapter;
import com.xp.intelli14.deku.adapter.TrendingBannerAdapter;
import com.xp.intelli14.deku.db.DBHandler;
import com.xp.intelli14.deku.model.ResponseGen;
import com.xp.intelli14.deku.property.StaticMemory;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFrag#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFrag extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public HomeFrag() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFrag.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFrag newInstance(String param1, String param2) {
        HomeFrag fragment = new HomeFrag();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    private RecyclerView trendingBannerRecylerView, currentPlayingRecylerView;
    private StaticMemory staticMemory = StaticMemory.getInstance();
    private TextView trendingTitleSeeMore, currentlyPlayingSeeMore;
    private ResponseGen trendingResponse, nowPlayingResponse;
    private Gson gson = new Gson();
    private DBHandler dbHandler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_home, container, false);
        dbHandler = new DBHandler(getActivity());

        String trendingData = dbHandler.getKeyViaId(staticMemory.CACHED_TRENDING_ID);
        trendingResponse = gson.fromJson(trendingData, ResponseGen.class);

        String nowPlayingData = dbHandler.getKeyViaId(staticMemory.CACHED_NOW_PLAYING_ID);
        nowPlayingResponse = gson.fromJson(nowPlayingData, ResponseGen.class);
        fillTrendingBanner(view);
        fillNowPlayingList(view);
        return view;
    }

    private void fillTrendingBanner(View view){
        trendingBannerRecylerView = view.findViewById(R.id.trendingBannerRecylerView);
        TrendingBannerAdapter adapter = new TrendingBannerAdapter(trendingResponse.results, getContext());
        trendingBannerRecylerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
        trendingBannerRecylerView.setAdapter(adapter);
        SnapHelper helper = new PagerSnapHelper();
        helper.attachToRecyclerView(trendingBannerRecylerView);
        trendingTitleSeeMore = view.findViewById(R.id.trendingTitleSeeMore);
        trendingTitleSeeMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                staticMemory.whatToShowOnShowMore = "Trending movies";
                startActivity(new Intent(getContext(), ShowMore.class));
            }
        });
    }

    private void fillNowPlayingList(View view){
        currentPlayingRecylerView = view.findViewById(R.id.currentPlayingRecylerView);
        NowPlayingAdapter adapter = new NowPlayingAdapter(nowPlayingResponse.results, getContext());
        currentPlayingRecylerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
        currentPlayingRecylerView.setAdapter(adapter);
        currentlyPlayingSeeMore = view.findViewById(R.id.currentlyPlayingSeeMore);
        currentlyPlayingSeeMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                staticMemory.whatToShowOnShowMore = "Currently playing";
                startActivity(new Intent(getContext(), ShowMore.class));
            }
        });
    }

}