package com.xp.intelli14.deku.model;

import java.util.List;

public class ResponseGen {
    public Dates dates;
    public int page;
    public List<Result> results;
    public int total_pages;
    public int total_results;
}
