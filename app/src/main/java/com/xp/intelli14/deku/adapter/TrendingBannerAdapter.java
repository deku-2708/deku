package com.xp.intelli14.deku.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.xp.intelli14.deku.R;
import com.xp.intelli14.deku.activity.OneMovie;
import com.xp.intelli14.deku.model.Result;
import com.xp.intelli14.deku.property.StaticMemory;

import java.util.List;

public class TrendingBannerAdapter extends RecyclerView.Adapter<TrendingBannerAdapter.ViewHolder>{

    private List<Result> results;
    private StaticMemory staticMemory = StaticMemory.getInstance();
    private Context context;

    public TrendingBannerAdapter(List<Result> results, Context context) {
        this.results = results;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView trendingBannerItemImageView;
        CardView trendingBannerItemCardView;
        TextView trendingBannerMovieTitle;
        View trendingGradView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            trendingBannerItemCardView = itemView.findViewById(R.id.trendingBannerItemCardView);
            trendingBannerItemImageView = itemView.findViewById(R.id.trendingBannerItemImageView);
            trendingBannerMovieTitle = itemView.findViewById(R.id.trendingBannerMovieTitle);
            trendingGradView = itemView.findViewById(R.id.trendingGradView);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.trending_banner_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        if(results.get(position).backdrop_path != null && !results.get(position).backdrop_path.isEmpty())
            Picasso.get().load(staticMemory.INITIAL_IMAGE_URL.concat(results.get(position).backdrop_path)).into(holder.trendingBannerItemImageView);
        holder.trendingBannerMovieTitle.setText(results.get(position).title);
        holder.trendingBannerItemCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                staticMemory.currentlyViewableMovie = results.get(position);
                context.startActivity(new Intent(context, OneMovie.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return results.size();
    }


}
