package com.xp.intelli14.deku.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.xp.intelli14.deku.R;
import com.xp.intelli14.deku.activity.OneMovie;
import com.xp.intelli14.deku.model.Result;
import com.xp.intelli14.deku.property.StaticMemory;

import java.util.List;

public class NowPlayingAdapter extends RecyclerView.Adapter<NowPlayingAdapter.ViewHolder>{

    private List<Result> results;
    private StaticMemory staticMemory = StaticMemory.getInstance();
    private Context context;

    public NowPlayingAdapter(List<Result> results, Context context) {
        this.results = results;
        this.staticMemory = staticMemory;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        CardView nowPlayingCardView;
        ImageView nowPlayingMoviePoster;
        View nowPlayingGradiant;
        TextView nowPlayingMovieTitle;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nowPlayingCardView = itemView.findViewById(R.id.nowPlayingCardView);
            nowPlayingMoviePoster = itemView.findViewById(R.id.nowPlayingMoviePoster);
            nowPlayingGradiant = itemView.findViewById(R.id.nowPlayingGradiant);
            nowPlayingMovieTitle = itemView.findViewById(R.id.nowPlayingMovieTitle);
        }
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.now_playing_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        Result currentMovie = results.get(position);
        if(currentMovie.poster_path != null && !currentMovie.poster_path.isEmpty())
            Picasso.get().load(staticMemory.INITIAL_IMAGE_URL.concat(currentMovie.poster_path)).into(holder.nowPlayingMoviePoster);

        holder.nowPlayingGradiant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                staticMemory.currentlyViewableMovie = results.get(position);
                context.startActivity(new Intent(context, OneMovie.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return results.size();
    }


}
