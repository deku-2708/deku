package com.xp.intelli14.deku.property;

import com.xp.intelli14.deku.model.ResponseGen;
import com.xp.intelli14.deku.model.Result;

import okhttp3.OkHttpClient;

public class StaticMemory {

    private static StaticMemory staticMemory = null;
    public String PAGE_NUMBER_PARAM = "{page_number}";
    public String DURATION_PARAM = "{duration}";
    public String SEARCH_STRING = "{search_string}";
    public String TRENDING_URL = "https://api.themoviedb.org/3/trending/all/{duration}?api_key=2dfb77b5993e4d52e010a58d032d1e37&page={page_number}";
    public String NOW_PLAYING = "https://api.themoviedb.org/3/movie/now_playing?api_key=2dfb77b5993e4d52e010a58d032d1e37&language=en-US&page={page_number}";
    public String SEARCH_URL = "https://api.themoviedb.org/3/search/movie?api_key=2dfb77b5993e4d52e010a58d032d1e37&language=en-US&page={page_number}&include_adult=true&query={search_string}";
    public OkHttpClient client = new OkHttpClient();
    public String INITIAL_IMAGE_URL = "https://image.tmdb.org/t/p/w500";
    public Result currentlyViewableMovie = null;
    public String whatToShowOnShowMore = null;
    public String CACHED_TRENDING_DATA = "CACHED_TRENDING_DATA";
    public int CACHED_TRENDING_ID = 1;

    public String CACHED_NOW_PLAYING_DATA = "CACHED_NOW_PLAYING_DATA";
    public int CACHED_NOW_PLAYING_ID = 2;

    public static StaticMemory getInstance(){
        if(staticMemory != null)
            return staticMemory;
        staticMemory = new StaticMemory();
        return staticMemory;
    }


}
