package com.xp.intelli14.deku.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.gson.Gson;
import com.xp.intelli14.deku.model.Result;

import java.util.ArrayList;
import java.util.List;

public class DBHandler extends SQLiteOpenHelper {

    private static final String DB_NAME = "dekuDB";

    // below int is our database version
    private static final int DB_VERSION = 1;

    // below variable is for our table name.
    private static final String TABLE_NAME = "config_data";

    private static final String SAVED_DB = "saved_db";

    // below variable is for our id column.
    private static final String ID_COL = "id";

    // below variable is for our course name column
    private static final String KEY_COL = "keyName";

    // below variable id for our course duration column.
    private static final String VALUE_COL = "valueName";

    public DBHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // on below line we are creating
        // an sqlite query and we are
        // setting our column names
        // along with their data types.
        String query = new StringBuilder("CREATE TABLE ")
                .append(TABLE_NAME).append(" (\n")
                .append(ID_COL).append(" PRIMARY KEY, \n")
                .append(KEY_COL).append(" TEXT, \n")
                .append(VALUE_COL).append(" TEXT )\n")
                .toString();

        String query2 = new StringBuilder("CREATE TABLE ")
                .append(SAVED_DB).append(" (\n")
                .append(ID_COL).append(" PRIMARY KEY, \n")
                .append(KEY_COL).append(" TEXT, \n")
                .append(VALUE_COL).append(" TEXT )\n")
                .toString();

        // at last we are calling a exec sql
        // method to execute above sql query
        db.execSQL(query);
        db.execSQL(query2);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // this method is called to check if the table exists already.
        //db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        //onCreate(db);
    }

    public boolean addOne(int id, String key, String value) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ID_COL, id);
        contentValues.put(KEY_COL, key);
        contentValues.put(VALUE_COL, value);

        long insert = db.insert(TABLE_NAME, null, contentValues);
        if(insert == -1){
            return false;
        }
        return true;
    }

    public String getKeyViaId(int id){
        String query = "SELECT * FROM "
                .concat(TABLE_NAME)
                .concat(" WHERE id = ")
                .concat(String.valueOf(id))
                .concat(";");

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToFirst()){
            int resId = cursor.getInt(0);
            String key = cursor.getString(1);
            String value = cursor.getString(2);

            return value;
        }
        return "";
    }


    public boolean updateOne(int id, String key, String value) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_COL, key);
        contentValues.put(VALUE_COL, value);


        long insert = db.update(TABLE_NAME, contentValues, ID_COL+" = "+id,null);

        if(insert == -1){
            return false;
        }
        return true;
    }



    public boolean addOneSaved(int id, String key, String value) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ID_COL, id);
        contentValues.put(KEY_COL, key);
        contentValues.put(VALUE_COL, value);

        long insert = db.insert(SAVED_DB, null, contentValues);
        if(insert == -1){
            return false;
        }
        return true;
    }

    public String getKeyViaIdSaved(int id){
        String query = "SELECT * FROM "
                .concat(SAVED_DB)
                .concat(" WHERE id = ")
                .concat(String.valueOf(id))
                .concat(";");

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToFirst()){
            int resId = cursor.getInt(0);
            String key = cursor.getString(1);
            String value = cursor.getString(2);

            return value;
        }
        return "";
    }


    public boolean updateOneSaved(int id, String key, String value) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_COL, key);
        contentValues.put(VALUE_COL, value);


        long insert = db.update(SAVED_DB, contentValues, ID_COL+" = "+id,null);

        if(insert == -1){
            return false;
        }
        return true;
    }
    private Gson gson = new Gson();
    public ArrayList<Result> getAllSavedMovies(){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM "
                .concat(SAVED_DB)
                .concat(";");
        Cursor cursor = db.rawQuery(query, null);
        ArrayList<Result> answer = new ArrayList<>();
        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            int id = cursor.getInt(0);
            String key = cursor.getString(1);
            String value = cursor.getString(2);
            answer.add(gson.fromJson(value, Result.class));
            cursor.moveToNext();
        }

        return answer;
    }
}
