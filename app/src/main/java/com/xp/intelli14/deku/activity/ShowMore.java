package com.xp.intelli14.deku.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.TextView;

import com.google.gson.Gson;
import com.xp.intelli14.deku.R;
import com.xp.intelli14.deku.adapter.GenMovieListAdapter;
import com.xp.intelli14.deku.db.DBHandler;
import com.xp.intelli14.deku.model.ResponseGen;
import com.xp.intelli14.deku.property.StaticMemory;

public class ShowMore extends AppCompatActivity {

    private RecyclerView showMoreRecylerView;
    private TextView showMoreTitle;
    private StaticMemory staticMemory = StaticMemory.getInstance();
    private Gson gson = new Gson();
    private DBHandler dbHandler;

    private void init(){
        showMoreRecylerView = findViewById(R.id.showMoreRecylerView);
        showMoreTitle = findViewById(R.id.showMoreTitle);
        showMoreTitle.setText(staticMemory.whatToShowOnShowMore);
        ResponseGen whatToShow = null;
        if(staticMemory.whatToShowOnShowMore.equals("Trending movies")){
            dbHandler = new DBHandler(this);
            String trendingData = dbHandler.getKeyViaId(staticMemory.CACHED_TRENDING_ID);
            whatToShow = gson.fromJson(trendingData, ResponseGen.class);
        }else{
            dbHandler = new DBHandler(this);
            String trendingData = dbHandler.getKeyViaId(staticMemory.CACHED_NOW_PLAYING_ID);
            whatToShow = gson.fromJson(trendingData, ResponseGen.class);
        }

        GenMovieListAdapter adapter = new GenMovieListAdapter(whatToShow.results, this);
        showMoreRecylerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        showMoreRecylerView.setAdapter(adapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_more);

        init();
    }
}