package com.xp.intelli14.deku.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.xp.intelli14.deku.R;
import com.xp.intelli14.deku.adapter.GenMovieListAdapter;
import com.xp.intelli14.deku.db.DBHandler;
import com.xp.intelli14.deku.model.ResponseGen;
import com.xp.intelli14.deku.model.Result;
import com.xp.intelli14.deku.property.StaticMemory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SavedFrag#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SavedFrag extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public SavedFrag() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SavedFrag.
     */
    // TODO: Rename and change types and number of parameters
    public static SavedFrag newInstance(String param1, String param2) {
        SavedFrag fragment = new SavedFrag();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    private Gson gson = new Gson();
    private DBHandler dbHandler;
    private RecyclerView savedRecyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_saved, container, false);
        dbHandler = new DBHandler(getActivity());
        ArrayList<Result> results = dbHandler.getAllSavedMovies();
        Collections.reverse(results);
        savedRecyclerView = view.findViewById(R.id.savedRecyclerView);
        GenMovieListAdapter adapter = new GenMovieListAdapter(results, getActivity());
        savedRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        savedRecyclerView.setAdapter(adapter);
        return view;
    }



}