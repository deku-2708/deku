package com.xp.intelli14.deku.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import com.xp.intelli14.deku.R;
import com.xp.intelli14.deku.fragment.HomeFrag;
import com.xp.intelli14.deku.fragment.SavedFrag;
import com.xp.intelli14.deku.fragment.SearchFrag;

import java.util.ArrayList;

public class Dashboard extends AppCompatActivity {

    private BottomNavigationView dashBoardBottomNavigation;
    private final HomeFrag homeFrag = new HomeFrag();
    private final SearchFrag searchFrag = new SearchFrag();

    private void init(){
        dashBoardBottomNavigation = findViewById(R.id.dashBoardBottomNavigation);
        openFragment(homeFrag);
        dashBoardBottomNavigation.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.menu_home:
                        openFragment(homeFrag);
                        return true;
                    case R.id.menu_search:
                        openFragment(searchFrag);
                        return true;
                    case R.id.menu_bookmark:
                        openFragment(new SavedFrag());
                        return true;
                    default:
                        return false;
                }

            }
        });
    }

    public void openFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.dashBoardFrameLayout, fragment);
        transaction.commit();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        init();
    }
}