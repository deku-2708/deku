package com.xp.intelli14.deku;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.Toast;

import com.google.gson.Gson;
import com.xp.intelli14.deku.activity.Dashboard;
import com.xp.intelli14.deku.db.DBHandler;
import com.xp.intelli14.deku.model.ResponseGen;
import com.xp.intelli14.deku.property.StaticMemory;

import java.io.IOException;
import java.util.HashSet;
import java.util.Objects;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    private StaticMemory staticMemory = StaticMemory.getInstance();;
    private Gson gson = new Gson();
    private HashSet<Integer> requestCompleteSet = new HashSet<>();
    private DBHandler dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbHandler = new DBHandler(this);


        if(isNetworkAvailable()){
            requestCompleteSet.add(1);
            getTrendingPageData("day", "10", 1);
            requestCompleteSet.add(2);
            getNowPlayingData("3", 2);
        }else{
            Toast.makeText(this, "Internet not available", Toast.LENGTH_LONG).show();
            String doesExsist1 = dbHandler.getKeyViaId(staticMemory.CACHED_TRENDING_ID);
            String doesExsist2 = dbHandler.getKeyViaId(staticMemory.CACHED_NOW_PLAYING_ID);
            if (doesExsist1.isEmpty() || doesExsist2.isEmpty()){
                Toast.makeText(this, "No offline data available", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(this, "Switching to offline mode", Toast.LENGTH_LONG).show();
                updateUI();
            }
        }
    }

    private void getTrendingPageData(String duration, String pageNumber, int requestNumber){
        String uri = staticMemory.TRENDING_URL
                .replace(staticMemory.DURATION_PARAM, duration)
                .replace(staticMemory.PAGE_NUMBER_PARAM, pageNumber);
        Request request = new Request.Builder()
                .url(uri)
                .build();
        Call call = staticMemory.client.newCall(request);

        call.enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplication(), e.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String doesExsist = dbHandler.getKeyViaId(staticMemory.CACHED_TRENDING_ID);
                            if(doesExsist.isEmpty()){
                                boolean success = dbHandler.addOne(staticMemory.CACHED_TRENDING_ID, staticMemory.CACHED_TRENDING_DATA, response.body().string());
                                requestCompleteSet.remove(requestNumber);
                                updateUI();
                            }else{
                                boolean success = dbHandler.updateOne(staticMemory.CACHED_TRENDING_ID, staticMemory.CACHED_TRENDING_DATA, response.body().string());
                                requestCompleteSet.remove(requestNumber);
                                updateUI();
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

    }

    private void getNowPlayingData(String pageNumber, int requestNumber){
        String uri = staticMemory.NOW_PLAYING
                .replace(staticMemory.PAGE_NUMBER_PARAM, pageNumber);
        Request request = new Request.Builder()
                .url(uri)
                .build();

        Call call = staticMemory.client.newCall(request);

        call.enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplication(), e.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String doesExsist = dbHandler.getKeyViaId(staticMemory.CACHED_NOW_PLAYING_ID);
                            if(doesExsist.isEmpty()){
                                boolean success = dbHandler.addOne(staticMemory.CACHED_NOW_PLAYING_ID, staticMemory.CACHED_NOW_PLAYING_DATA, response.body().string());
                                requestCompleteSet.remove(requestNumber);
                                updateUI();
                            }else{
                                boolean success = dbHandler.updateOne(staticMemory.CACHED_NOW_PLAYING_ID, staticMemory.CACHED_NOW_PLAYING_DATA, response.body().string());
                                requestCompleteSet.remove(requestNumber);
                                updateUI();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }

    private void updateUI(){
        if(requestCompleteSet.isEmpty()){
            startActivity(new Intent(MainActivity.this, Dashboard.class));
            finish();
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}