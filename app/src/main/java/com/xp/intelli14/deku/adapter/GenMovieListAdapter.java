package com.xp.intelli14.deku.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.xp.intelli14.deku.R;
import com.xp.intelli14.deku.activity.OneMovie;
import com.xp.intelli14.deku.model.Result;
import com.xp.intelli14.deku.property.StaticMemory;

import java.util.List;

public class GenMovieListAdapter extends RecyclerView.Adapter<GenMovieListAdapter.ViewHolder>{


    private List<Result> results;
    private StaticMemory staticMemory = StaticMemory.getInstance();
    private Context context;

    public GenMovieListAdapter(List<Result> results, Context context) {
        this.results = results;
        this.context = context;
    }

    public class ViewHolder  extends RecyclerView.ViewHolder {

        private CardView genItemCardView;
        private ImageView movieGenImageView, movieGenMoviePoster;
        private View movieGenGradiant;
        private RelativeLayout movieGenRelative;
        private TextView movieGenTitle,movieGenDesc;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            genItemCardView = itemView.findViewById(R.id.genItemCardView);
            movieGenImageView = itemView.findViewById(R.id.movieGenImageView);
            movieGenMoviePoster = itemView.findViewById(R.id.movieGenMoviePoster);
            movieGenGradiant= itemView.findViewById(R.id.movieGenGradiant);
            movieGenRelative= itemView.findViewById(R.id.movieGenRelative);
            movieGenTitle= itemView.findViewById(R.id.movieGenTitle);
            movieGenDesc= itemView.findViewById(R.id.movieGenDesc);
        }
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_gen_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        Result currentMovie = results.get(position);
        if(currentMovie.backdrop_path != null && !currentMovie.backdrop_path.isEmpty()){
            Picasso.get().load(staticMemory.INITIAL_IMAGE_URL.concat(currentMovie.backdrop_path)).into(holder.movieGenImageView);
        }
        if(currentMovie.poster_path != null && !currentMovie.poster_path.isEmpty()){
            Picasso.get().load(staticMemory.INITIAL_IMAGE_URL.concat(currentMovie.poster_path)).into(holder.movieGenMoviePoster);
        }
        int overLimit = 200;
        holder.movieGenTitle.setText(currentMovie.title);
        if(currentMovie.overview.length() > overLimit){
            holder.movieGenDesc.setText(currentMovie.overview.substring(0,overLimit-3).concat("..."));
        }else{
            holder.movieGenDesc.setText(currentMovie.overview);
        }
        holder.movieGenGradiant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                staticMemory.currentlyViewableMovie = results.get(position);
                context.startActivity(new Intent(context, OneMovie.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return results.size();
    }


}
